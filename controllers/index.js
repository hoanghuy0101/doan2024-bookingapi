import userController from "./user.js";
import roomsListController from "./roomsList.js";
import bookingRoomsController from "./bookingRooms.js";
import countryController from "./country.js";
import discountController from './discount.js'
import commentsController from './comments.js'
import referralController from './referral.js'
import roomtypeController from './roomType.js'
import serviceController from './service.js'

export {
    userController,
    roomsListController,
    bookingRoomsController,
    countryController,
    discountController,
    commentsController,
    referralController,
    roomtypeController,
    serviceController
}
