import HttpStatusCode from "../exception/HttpStatusCode.js";
import roomType from "../repositories/RoomType.js";
import { MAX_RECORDS } from "../Global/constants.js";

const getRoomTypelist = async (req, res) => {
  let { page = 1, size = MAX_RECORDS, keyword = "" } = req.query;
  size = parseInt(size) >= MAX_RECORDS ? MAX_RECORDS : parseInt(size);
  page = parseInt(page);
  try {
    let totalRecords = await roomType.CountData(keyword);

    let numberOfPages = Math.ceil(totalRecords / size);

    let filteredRoomType = await roomType.getAllRoomType({
      size,
      page,
      keyword: keyword,
    });

    res.status(HttpStatusCode.OK).json({
      message: "get roomtype successfully",
      size: size,
      keyword,
      page,
      numberOfPages,
      data: filteredRoomType,
    });
  } catch (exception) {
    res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
      message: exception.message,
    });
  }
};

const getRoomTypeById = async (req, res) => {
  debugger;
  let roomTypeId = req.params.id;
  try {
    const detailRoomType = roomType.getDetailRoomType(roomTypeId);
    debugger;
    res.status(HttpStatusCode.OK).json({
      message: "get roomtype sucessfully",
      data: detailRoomType,
    });
  } catch (exception) {
    res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
      message: exception.message,   
    });
  }
};

export default {
  getRoomTypelist,
  getRoomTypeById,  
};
