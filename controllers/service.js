import HttpStatusCode from "../exception/HttpStatusCode.js";
import Service from "../repositories/service.js";
import { MAX_RECORDS } from "../Global/constants.js";

const getServicelist = async (req, res) => {
  let { page = 1, size = MAX_RECORDS, keyword = "" } = req.query;
  size = parseInt(size) >= MAX_RECORDS ? MAX_RECORDS : parseInt(size);
  page = parseInt(page);
  try {
    let totalRecords = await Service.CountData(keyword);

    let numberOfPages = Math.ceil(totalRecords / size);

    let filteredService = await Service.getAllservice({
      size,
      page,
      keyword: keyword,
    });

    res.status(HttpStatusCode.OK).json({
      message: "get Service successfully",
      size: size,
      keyword,
      page,
      numberOfPages,
      data: filteredService,
    });
  } catch (exception) {
    res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
      message: exception.message,
    });
  }
};

const getServiceById = async (req, res) => {
  debugger;
  let ServiceId = req.params.id;
  try {
    const detailService = Service.getDetailService(ServiceId);
    debugger;
    res.status(HttpStatusCode.OK).json({
      message: "get Service sucessfully",
      data: detailService,
    });
  } catch (exception) {
    res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
      message: exception.message,   
    });
  }
};

export default {
  getServicelist,
  getServiceById,  
};
