import HttpStatusCode from "../exception/HttpStatusCode.js";
import Roomlist from "../repositories/roomsList.js";
import { RoomsList } from "../models/index.js";
import { BookingRoom } from "../models/index.js";
import { MAX_RECORDS } from "../Global/constants.js";

const getListRoomslist = async (req, res) => {
  if (req.query.id) {
    const roomId = req.query.id;
    if (roomId && roomId !== "undefined") {
      const room = await RoomsList.findById(roomId);
      try {
        if (!room) {
          return res.status(404).json({ error: "Room not found" });
        }
        res.status(HttpStatusCode.OK).json({
          message: "get detail sucessfully",
          data: room,
        });
      } catch (error) {
        return res.status(404).json({ error: "Room not found" });
      }
    } else {
      return res.status(404).json({ error: "Room not found" });
    }
  } else {
    let { page = 1, size = MAX_RECORDS, keyword = "" } = req.query;
    size = parseInt(size) >= MAX_RECORDS ? MAX_RECORDS : parseInt(size);
    page = parseInt(page);
    try {
      let totalRecords = await Roomlist.CountData(keyword);

      let numberOfPages = Math.ceil(totalRecords / size);

      let filteredRoomslist = await Roomlist.getAllTRoomsList({
        size,
        page,
        keyword: keyword,
      });

      res.status(HttpStatusCode.OK).json({
        message: "get rooms successfully",
        size: size,
        keyword,
        page,
        numberOfPages,
        data: filteredRoomslist,
      });
    } catch (exception) {
      res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
        message: exception.message,
      });
    }
  }
};

const getListRoomslistById = async (req, res) => {
  let RoomsListId = req.params.id;
  try {
    const detailRooms = Roomlist.getDetailRoomslist(RoomsListId);
    res.status(HttpStatusCode.OK).json({
      message: "get rooms sucessfully",
      data: detailRooms,
    });
  } catch (exception) {
    res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
      message: exception.message,
    });
  }
};

const getFeaturedRooms = async (req, res) => {
  try {
    const featuredRooms = await Roomlist.getFeaturedRooms();
    res.status(HttpStatusCode.OK).json({
      message: "get Featured Rooms Sucessfully",
      data: featuredRooms,
    });
  } catch (exception) {
    res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
      message: exception.message,
    });
  }
};

const getTrendingRooms = async (req, res) => {
  try {
    const TredingRooms = await Roomlist.getTrendingRooms();
    res.status(HttpStatusCode.OK).json({
      message: "get Featured Rooms Sucessfully",
      data: TredingRooms,
    });
  } catch (exception) {
    res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
      message: exception.message,
    });
  }
};

async function findAvailableRooms(
  checkInDate,
  checkOutDate,
  type,
  services,
  guests,
  numRooms
) {
  try {
    // Chuyển đổi ngày check-in và check-out thành đối tượng Date
    const checkIn = new Date(checkInDate);
    const checkOut = new Date(checkOutDate);

    // Tìm các phòng đã được đặt trong khoảng thời gian này
    const bookedRooms = await BookingRoom.find({
      status: "confirmed",
      $or: [{ checkIn: { $lt: checkOut }, checkOut: { $gt: checkIn } }],
    }).exec();

    // Lấy danh sách roomId của các phòng đã đặt
    const bookedRoomIds = bookedRooms.map((booking) => booking.roomId);

    // Tạo điều kiện tìm kiếm
    let searchCriteria = { _id: { $nin: bookedRoomIds } };

    if (type) {
      searchCriteria.type = type;
    }

    if (services) {
      const servicesArray = services
        .split(",")
        .map((service) => service.trim());
      searchCriteria.service = { $all: servicesArray };
    }

    if (guests) {
      searchCriteria.MaximumOfGuests = { $gte: guests };
    }

    // Tìm các phòng không nằm trong danh sách đã đặt và theo tiêu chí tìm kiếm
    let availableRooms = await RoomsList.find(searchCriteria).exec();

    // Sắp xếp các phòng theo MaximumOfGuests tăng dần
    availableRooms.sort((a, b) => a.MaximumOfGuests - b.MaximumOfGuests);

    // Nếu không đủ phòng cho số lượng phòng yêu cầu, chọn các phòng phù hợp nhất
    // if (numRooms && numRooms > 0) {
    //   let selectedRooms = [];
    //   let remainingGuests = guests * numRooms;

    //   for (let room of availableRooms) {
    //     if (selectedRooms.length < numRooms) {
    //       selectedRooms.push(room);
    //       remainingGuests -= room.MaximumOfGuests;
    //     } else {
    //       break;
    //     }
    //   }

    //   // Nếu vẫn còn khách chưa được phục vụ, chọn thêm các phòng khác
    //   if (remainingGuests > 0) {
    //     for (let room of availableRooms) {
    //       if (!selectedRooms.includes(room)) {
    //         selectedRooms.push(room);
    //         remainingGuests -= room.MaximumOfGuests;
    //         if (remainingGuests <= 0) {
    //           break;
    //         }
    //       }
    //     }
    //   }

    //   availableRooms = selectedRooms;
    // }
    return availableRooms;
  } catch (err) {
    throw new Error("Error fetching available rooms: " + err.message);
  }
}

export default {
  getListRoomslist,
  getListRoomslistById,
  getFeaturedRooms,
  getTrendingRooms,
  findAvailableRooms,
};
