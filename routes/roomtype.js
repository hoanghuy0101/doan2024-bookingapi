import express from "express";
const router = express.Router();
import { roomtypeController } from "../controllers/index.js";
import { RoomType } from "../models/index.js";

router.get("/", roomtypeController.getRoomTypelist);

router.get("/:id", (req, res) => {
  const roomId = req.params.id;

  RoomType.findById(roomId, (err, tour) => {
    if (err) {
      res.status(500).json({ error: err.message });
    } else if (!tour) {
      res.status(404).json({ error: "Tour not found" });
    } else {
      res.json(tour);
    }
  });
});
router.delete("/:id", (req, res) => {
  const roomId = req.params.id;

  RoomType.findByIdAndDelete(roomId, (err, tour) => {
    if (err) {
      res.status(500).json({ error: err.message });
    } else if (!tour) {
      res.status(404).json({ error: "roomtype not found" });
    } else {
      res.json({ success: true, message: "roomtype deleted successfully" });
    }
  });
});

router.post("/insert", (req, res) => {
  const { name } = req.body;

  const newRoom = new RoomType({
    name,
  });
  newRoom.save((err, Roomtype) => {
    if (err) {
      debugger;
      res.status(500).json({ success: false, message: err.message });
    } else {
      res.json({ success: true, message: "RoomType inser success", Roomtype });
    }
  });
});

router.post("/update", async (req, res) => {
  const { id, name } = req.body;
  try {
    const rooms = await RoomType.findById(id);
    if (!rooms) {
      return res
        .status(404)
        .json({ success: false, message: `Roomstype not found ${name}` });
    }
    rooms.name = name ?? rooms.name;
    await rooms.save();

    res.json({ success: true, message: "Roomstype update success", rooms });
  } catch (err) {
    res.status(500).json({ success: false, message: err.message });
  }
});

export default router;
