import express from "express";
const router = express();
import { bookingRoomsController } from "../controllers/index.js";

router.post('/', bookingRoomsController.insertbooking);
router.get('/getall', bookingRoomsController.getListBookingRoom);

export default router;
