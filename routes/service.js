import express from "express";
const router = express.Router();
import { serviceController } from "../controllers/index.js";
import { service } from "../models/index.js";

router.get("/", serviceController.getServicelist);

router.get("/:id", (req, res) => {
  const serviceId = req.params.id;

  service.findById(serviceId, (err, tour) => {
    if (err) {
      res.status(500).json({ error: err.message });
    } else if (!tour) {
      res.status(404).json({ error: "Tour not found" });
    } else {
      res.json(tour);
    }
  });
});
router.delete("/:id", (req, res) => {
  const serviceId = req.params.id;

  service.findByIdAndDelete(serviceId, (err, tour) => {
    if (err) {
      res.status(500).json({ error: err.message });
    } else if (!tour) {
      res.status(404).json({ error: "service not found" });
    } else {
      res.json({ success: true, message: "service deleted successfully" });
    }
  });
});

router.post("/insert", (req, res) => {
  const { name } = req.body;

  const newRoom = new service({
    name,
  });
  newRoom.save((err, service) => {
    if (err) {
      debugger;
      res.status(500).json({ success: false, message: err.message });
    } else {
      res.json({ success: true, message: "service inser success", service });
    }
  });
});

router.post("/update", async (req, res) => {
  const { id, name } = req.body;
  try {
    const services = await service.findById(id);
    if (!services) {
      return res
        .status(404)
        .json({ success: false, message: `services not found ${name}` });
    }
    services.name = name ?? services.name;
    await services.save();

    res.json({ success: true, message: "services update success", services });
  } catch (err) {
    res.status(500).json({ success: false, message: err.message });
  }
});

export default router;
