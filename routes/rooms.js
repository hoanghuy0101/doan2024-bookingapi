import express from "express";
const router = express.Router();
import { roomsListController } from "../controllers/index.js";
import { RoomsList } from "../models/index.js";
import multer from "multer";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "public/uploads/");
  },
  filename: (req, file, cb) => {
    const filename = Date.now() + "-" + file.originalname;
    cb(null, filename);
  },
});

const upload = multer({ storage });

router.get("/", roomsListController.getListRoomslist);

router.get("/featuredList", roomsListController.getFeaturedRooms);

router.get("/trendingRoom", roomsListController.getTrendingRooms);

router.get("/available-rooms", async (req, res) => {
  const { checkIn, checkOut, type, services, guests, numRooms } = req.query;

  if (!checkIn || !checkOut) {
    return res
      .status(400)
      .json({
        success: false,
        message: "Please provide both check-in and check-out dates.",
      });
  }

  try {
    const availableRooms = await roomsListController.findAvailableRooms(
      checkIn,
      checkOut,
      type,
      services,
      parseInt(guests),
      parseInt(numRooms)
    );
    res.json({ success: true, rooms: availableRooms });
  } catch (err) {
    res.status(500).json({ success: false, message: err.message });
  }
});

router.delete("/:id", (req, res) => {
  const roomId = req.params.id;

  RoomsList.findByIdAndDelete(roomId, (err, room) => {
    if (err) {
      res.status(500).json({ error: err.message });
    } else if (!room) {
      res.status(404).json({ error: "room not found" });
    } else {
      res.json({ success: true, message: "room deleted successfully" });
    }
  });
});

router.post("/insert", upload.array("photos"), (req, res) => {
  const {
    name,
    type,
    numberOfFloors,
    status,
    bedType,
    title,
    desc,
    rating,
    priceForTheWeek,
    weekendprice,
    service,
    acreage,
    MaximumOfGuests,
  } = req.body;

  const serviceArray = JSON.parse(service);

  const photos = req.files.map((file) => {
    return `http://localhost:8880/public/uploads/${file.filename}`;
  });

  const newRoom = new RoomsList({
    name,
    type,
    numberOfFloors,
    status,
    bedType,
    title,
    desc,
    rating,
    priceForTheWeek,
    weekendprice,
    service: serviceArray,
    acreage,
    MaximumOfGuests: parseInt(MaximumOfGuests),
    photos,
  });
  newRoom.save((err, tour) => {
    if (err) {
      res.status(500).json({ success: false, message: err.message });
    } else {
      res.json({ success: true, message: "Rooms insert success", tour });
    }
  });
});

router.post("/update", upload.array("photos"), async (req, res) => {
  const {
    id,
    name,
    type,
    numberOfFloors,
    status,
    bedType,
    title,
    desc,
    rating,
    priceForTheWeek,
    weekendprice,
    service,
    acreage,
    MaximumOfGuests,
  } = req.body;

  const serviceArray = JSON.parse(service);

  try {
    const tour = await RoomsList.findById(id);
    if (!tour) {
      return res
        .status(404)
        .json({ success: false, message: `Room not found: ${name}` });
    }

    tour.name = name ?? tour.name;
    tour.type = type ?? tour.type;
    tour.numberOfFloors = numberOfFloors ?? tour.numberOfFloors;
    tour.status = status ?? tour.status;
    tour.bedType = bedType ?? tour.bedType;
    tour.title = title ?? tour.title;
    tour.desc = desc ?? tour.desc;
    tour.rating = rating ?? tour.rating;
    tour.priceForTheWeek = priceForTheWeek ?? tour.priceForTheWeek;
    tour.weekendprice = weekendprice ?? tour.weekendprice;
    tour.service = serviceArray ?? tour.service;
    tour.acreage = acreage ?? tour.acreage;
    tour.MaximumOfGuests =
      parseInt(MaximumOfGuests) ?? parseInt(tour.MaximumOfGuests);

    // Handle photos update
    if (req.files && req.files.length > 0) {
      const photos = req.files.map((file) => {
        return `http://localhost:8880/public/uploads/${file.filename}`;
      });
      tour.photos = photos;
    }

    await tour.save();
    res.json({ success: true, message: "Room update success", tour });
  } catch (err) {
    res.status(500).json({ success: false, message: err.message });
  }
});

export default router;
