import userRouter from './user.js'
import RoomsRouter from './rooms.js'
import bookingRouter from './bookingRooms.js'
import countryRouter from './country.js'
import discountRouter from './discount.js'
import commentsRouter from './comments.js'
import referralRouter from './referral.js'
import roomtypeRouter from './roomtype.js'
import serviceRouter from './service.js'

export {
    userRouter,
    RoomsRouter,
    bookingRouter,
    countryRouter,
    discountRouter,
    commentsRouter,
    referralRouter,
    roomtypeRouter,
    serviceRouter
}