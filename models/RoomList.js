import mongoose, { Schema, ObjectId } from "mongoose";

export default mongoose.model(
  "RoomsList",
  new Schema({
    id: { type: ObjectId },
    name: {
      type: String,
      required: true, // NOT NULL
      //   validate: {
      //     validator: (value) => value.length > 3,
      //     message: "Username must be at least 3 characters",
      //   },
    },
    type: {
      type: String,
      required: true,
    },
    numberOfFloors: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
    },
    bedType: {
      type: String,
      required: true,
    },
    photos: {
      type: [String],
    },
    title: {
      type: String,
      required: true,
    },
    desc: {
      type: String,
      required: true,
    },
    rating: {
      type: Number,
      min: 0,
      max: 5,
    },
    priceForTheWeek: {
      type: Number,
      required: true,
    },
    weekendprice: {
      type: Number,
      required: true,
    },
    service: {
      type: [String],
      required: true,
    },
    acreage: {
      type: String,
      required: true,
    },
    MaximumOfGuests: {
      type: String,
      required: true,
    },
  })
);
