import mongoose, { Schema, ObjectId } from "mongoose";

export default mongoose.model(
  "BookingRooms",
  new Schema({
    id: { type: ObjectId },
    roomId: {
      type: String,
      required: true, // NOT NULL
    },
    checkIn: {
      type: String,
      required: true,
    },
    checkOut: {
      type: String,
      required: true,
    },
    phoneNumber: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    numberPeople: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    userId: {
      type: String,
      required: true,
    },
    price: {
      type: String,
      required: true,
    },
    note: {
      type: String,
    },
    confirm: {
      type: Number,
    },
  })
);
