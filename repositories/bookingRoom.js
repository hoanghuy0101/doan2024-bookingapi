import { BookingRoom } from "../models/index.js";
import Exception from "../exception/Exception.js";

const Createbooking = async ({
  roomId,
  name,
  userId,
  price,
  note,
  phoneNumber,
  numberPeople,
  email,
  checkOut,
  checkIn,
}) => {
  try {
    debugger;
    const Booking = await BookingRoom.create({
      roomId,
      name,
      userId,
      price,
      note,
      phoneNumber,
      numberPeople,
      email,
      checkOut,
      checkIn,
      confirm: 0,
    });
    return Booking;
  } catch (exception) {
    if (!!exception.errors) {
      throw new Exception("input error: ", exception.errors);
    }
    debugger;
  }
  debugger;
};

const getAllBookingRoom = async ({ page, size, searchString }) => {
  page = parseInt(page);
  size = parseInt(size);
  //searchString? name, email, address constains searchString
  debugger;
  let filteredBookingRooms = await BookingRoom.aggregate([
    {
      $match: {
        // $or: [
        //     {
        //         name: {$regex: `.*${searchString}.*`}
        //     },
        //     {
        //         timeStart: {$regex: `.*${searchString}.*`}
        //     },
        //     {
        //         timeStart: {$regex: `.*${searchString}.*`}
        //     },
        // ]
      },
    },
    // {
    //     $skip: page - 1 * size
    // },
    { $limit: size },
  ]);
  return filteredBookingRooms;
};

export default {
  Createbooking,
  getAllBookingRoom,
};
