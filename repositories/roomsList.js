import { RoomsList } from "../models/index.js";
import { BookingRoom } from "../models/index.js";
import Exception from "../exception/Exception.js";

const getAllTRoomsList = async ({ page, size, keyword }) => {
  page = parseInt(page);
  size = parseInt(size);
  const skip = (page - 1) * size;

  let filteredRoomsList = await RoomsList.aggregate([
    {
      $match: {
        $or: [
          {
            name: { $regex: new RegExp(keyword, "i") },
          },
        ],
      },
    },
    { $skip: skip },
    { $limit: size },
  ]);
  return filteredRoomsList;
};

const getDetailRoomslist = async (RoomsListId) => {
  const Rooms = await RoomsList.findById(RoomsListId);
  if (!Rooms) {
    throw new Exception("cannot find Rooms by", RoomsListId);
  }
  return Rooms; // default value
};

const CountData = async (keyword) => {
  let totalRecords = await RoomsList.countDocuments({
    name: { $regex: new RegExp(keyword, "i") },
  });
  return totalRecords;
};

async function getFeaturedRooms() {
  try {
    const highRatedRooms = await RoomsList.find({
      rating: { $exists: true, $gte: 4.5 },
    }).exec();

    if (highRatedRooms.length > 0) {
      return highRatedRooms;
    } else {
      const luxuryRooms = await RoomsList.find({ type: "Phòng luxury" }).exec();
      return luxuryRooms;
    }
  } catch (err) {
    throw new Error("Error fetching featured rooms: " + err.message);
  }
}

async function getTrendingRooms() {
  try {
    const roomBookingCounts = await BookingRoom.aggregate([
      { $match: { status: "confirmed" } },
      { $group: { _id: "$roomId", count: { $sum: 1 } } },
      { $sort: { count: -1 } },
    ]).exec();
    if (roomBookingCounts.length > 0) {
      const trendingRoomIds = roomBookingCounts.map((room) => room._id);
      const trendingRooms = await RoomsList.find({
        _id: { $in: trendingRoomIds },
      }).exec();
      return trendingRooms;
    } else {
      const manualRooms = await RoomsList.find({ type: "Phòng manual" }).exec();
      return manualRooms;
    }
  } catch (error) {
    throw new Error('Error fetching trending rooms: ' + err.message);
  }
}

export default {
  getAllTRoomsList,
  getDetailRoomslist,
  CountData,
  getFeaturedRooms,
  getTrendingRooms,
};
