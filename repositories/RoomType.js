import { RoomType } from "../models/index.js";
import Exception from "../exception/Exception.js";  

const getAllRoomType = async ({ page, size, keyword }) => {
  page = parseInt(page);
  size = parseInt(size);
  const skip = (page - 1) * size;

  let filteredRoomType = await RoomType.aggregate([
    {
      $match: {
        $or: [
          {
            name: { $regex: new RegExp(keyword, "i") },
          },
        ],
      },
    },
    { $skip: skip },
    { $limit: size },
  ]);
  return filteredRoomType;
};

const getDetailRoomType = async (roomTypeId) => {
  const room = await RoomType.findById(roomTypeId);
  if (!room) {
    throw new Exception("cannot find roomtype by", roomTypeId);
  }
  return room; // default value
};

const CountData = async (keyword) => {
  let totalRecords = await RoomType.countDocuments({
    name: { $regex: new RegExp(keyword, "i") },
  });
  return totalRecords;
};

export default {
  getAllRoomType,
  getDetailRoomType,
  CountData,
};
