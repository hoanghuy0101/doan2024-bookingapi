import { service } from "../models/index.js";
import Exception from "../exception/Exception.js";  

const getAllservice = async ({ page, size, keyword }) => {
  page = parseInt(page);
  size = parseInt(size);
  const skip = (page - 1) * size;

  let filteredservice = await service.aggregate([
    {
      $match: {
        $or: [
          {
            name: { $regex: new RegExp(keyword, "i") },
          },
        ],
      },
    },
    { $skip: skip },
    { $limit: size },
  ]);
  return filteredservice;
};

const getDetailservice = async (serviceId) => {
  const services = await service.findById(serviceId);
  if (!services) {
    throw new Exception("cannot find service by", serviceId);
  }
  return services; // default value
};

const CountData = async (keyword) => {
  let totalRecords = await service.countDocuments({
    name: { $regex: new RegExp(keyword, "i") },
  });
  return totalRecords;
};

export default {
  getAllservice,
  getDetailservice,
  CountData,
};
