import roomsListRepositories from "./roomsList.js";
import userRepositories from './user.js'
import bookingRepositories from "./bookingRoom.js";
import country from "./country.js";
import discount from "./discount.js";
import comments from "./comments.js";
import roomType from "./RoomType.js";

export {
    roomsListRepositories,
    userRepositories,
    bookingRepositories,
    country,
    discount,
    comments,
    roomType
}